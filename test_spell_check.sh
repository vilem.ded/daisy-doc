#!/bin/bash
set -e

#set en_GB for British English
aspell_lang=en

echo "Spell checking"
spell_check_code=0
for x in $(find test/ -name '*.html');
do
	lines=`cat $x|aspell --lang=$aspell_lang --encoding=utf-8 --mode html --add-extra-dicts=./.aspell.en.pws list 2>errors.txt |wc -l`
	if [ $lines -gt 0 ]
	then
		echo $x:
		cat $x | aspell --lang=$aspell_lang --encoding=utf-8 --mode html --add-extra-dicts=./.aspell.en.pws list
		spell_check_code=1
		echo
	fi
# we need to check stderr - in case something really bad happens there will be output there
        lines=`cat errors.txt | wc -l`
	if [ $lines -gt 0 ]
	then
        	cat errors.txt
		spell_check_code=1
	fi
done

if [ $spell_check_code -gt 0 ]
then
	echo "Spell check found some problems. Either fix them or add exceptions to the dictionary file: .aspell.en.pws"
fi
rm errors.txt
exit $spell_check_code
