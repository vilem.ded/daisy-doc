---
layout: page
title: Contact us
permalink: /contact/
order: 4
---

# Contact and credits

For more details or information about DAISY application, contact us:

 - Primary contact: [Pinar Alper](mailto:pinar.alper@uni.lu)
 - Primary contact: [Regina Becker](mailto:regina.becker@uni.lu)

 - Developer: [Valentin Grouès](mailto:valentin.groues@uni.lu)
 - Developer: [Jacek Lebioda](mailto:jacek.lebioda@uni.lu)
 - Developer [Yohan Jarosz](mailto:yohan.jarosz@uni.lu)
 - Developer [Kavita Rege ](mailto:kavita.rege@uni.lu)

 - Contributor: [Vilem Ded](mailto:vilem.ded@uni.lu)
 - Contributor: [Sandrine Munoz](mailto:sandrine.munoz@uni.lu)
 - Contributor: [Christophe Trefois](mailto:christophe.trefois@uni.lu)
 - Contributor: [Venkata Satagopam](mailto:venkata.satagopam@uni.lu)
 - Contributor: [Reinhard Schneider](mailto:reinhard.schneider@uni.lu)

<br>
In case of problems with this website please contact [LCSB data stewards](mailto:lcsb-datastewards@uni.lu).
