
# Login to DAISY

Upon successful installation of DAISY, go to the web address
`https://${IP_ADDRESS_OR_NAME_OF_DEPLOYMENT_SERVER}`, where you should display the login page.  
<!-- If you are University of Luxembourg staff you can go to [https://daisy.lcsb.uni.lu/](https://daisy.lcsb.uni.lu/). -->
<!-- You can also check [DAISY demo deployment](https://daisy-demo.elixir-luxembourg.org/). -->


Based on the authentication configuration made for your deployment, you may log in by:
* the user definitions in an existing LDAP directory, e.g. institutional/uni credentials.
* the user definitions maintained within the DAISY database.

<br>
<span style="display:block;text-align:center">![Alt]({{ "img/login.png" | relative_url }}){:width="800px"}<br/><small>DAISY Login Page</small></span>
<br>

After successful login, you see DAISY home page.
<br>

<span style="display:block;text-align:center">![Alt]({{ "img/after_login.png" | relative_url }}){:width="800px"}<br/><small>DAISY Home Page</small></span>

---
<div style="text-align: right"> <strong><a href="#top">Back to top</a></strong></div>
<br />
