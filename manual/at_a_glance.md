
# 1 DAISY at a Glance
This section contains a brief description of DAISY functions listed in the application's menu bar (image below) and some tips how to effectively familiarise with DAISY application.     

<br>
<span style="display:block; text-align:center">![Alt](../img/menubar.png "DAISY Menu bar"){:width="800px"}<br/><small>DAISY Menu bar</small></span>
<br>

 * [_Datasets Management_](#4-dataset-management) module allows for the recording of personal data held by the institution. The dataset may or may not fall in the context of a particular project. DAISY allows datasets to be defined in a granular way; where - if desired - each data subset, called a *data declaration*, can be listed individually. These declarations may list data from a particular partner, data of a particular cohort or data of a particular type.

 * [_Projects Management_](#3-project-management) module allows for the recording of research activities as projects. Documenting projects is critical for GDPR compliance as projects constitute the purpose and the context of use of the personal data.    
 Any document supporting the legal and ethical basis for data use can be stored in DAISY (e.g. ethics approvals, consent configurations or subject information sheets).

 * [_Contracts Management_](#5-contract-management) module allows for the recording and storage of legal contracts of various types that have been signed with partner institutes or suppliers. Consortium agreements, data sharing agreements, material transfer agreements are the examples of the contracts.
 <!-- For GDPR compliance the contracts become useful when documenting the source of received datasets or the target datasets transferred. -->
 For GDPR compliance the contracts become useful in case of documenting the received datasets source or transferred datasets target.

 * [_Definitions Management_](#6-definitions-management) - DAISY comes pre-packed with the default lookup lists; these can be changed during the application deployment. Lookup lists are used when defining the contracts, projects or datasets. The definitions module of the DAISY application allows on the management of dynamic lookup lists, specifically those of the cohorts, partner institutes and contact persons.

<br />

<mark>Our suggestion to the first-time users:</mark>

  1. Familiarise yourself with DAISY's layout by reading the next section [Interface Conventions](#2-daisy-interface-conventions).  
  2. Login to DAISY as standard user and VIP user on your DAISY instance.
  3. Use Project Management to [create a project](#31-create-new-project).
  4. Use Dataset Management to [create a dataset]({{ "/manual/project_management_details/#321-add-project-dataset" | relative_url }}) within the defined project.

<!-- 1. Familiarise yourself with DAISY's interface conventions by reading the [relevant section of this guide](#DIC). -->
<!-- 2. Login to DAISY with a user that has Administrator or VIP privileges. E.g. In the demo deployment the _admin_ user has Administrator privileges and _alice.white_ has VIP privileges. -->
<!-- 3. Use Project Management to [create a project](#PM1). -->
<!-- 4. Use Dataset Management to [create a dataset](#PM21) within the defined project. -->

---
<div style="text-align: right"> <strong><a href="#top">Back to top</a></strong></div>
<br />
