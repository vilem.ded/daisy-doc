---
layout: page
title: User guide
permalink: /manual/
order: 2
---

# <a name="top"></a>DAISY User Guide
{:.no_toc}

Welcome to the user guide for the DAta Information SYstem (DAISY). DAISY is a tool that assists GDPR compliance by keeping a register of personal data used in research.



<br/>
* TOC
{:toc}
<br/>


{% include_relative quickstart.md %}
{% include_relative users_modules.md %}


